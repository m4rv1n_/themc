// Something 'bout those little pills, unreal, the thrills, they yield, until they kill a million braincells

var playerModule = (function() {
  var player;
  var devMode;
  var logBuffer = [];
  var isItemPlaying = false;
  var lastPercentile = 0;
  var lastTime = 0;
  var events = ["advertpause", "advertplay", "advertstarted", "announcementclicked", "announcementcloseclicked", "announcementhide", "itemended", "itempause", "itemplay", "itemskipped", "itemstarted", "mediaended", "mediastarted", "playlistended", "ready", "rewindclicked", "skinload", "sourcechange"];
  var options = {
    playlist: {
      items: []
    }
  };

  getAdTag = function(brandelli) {
    var tag;
    var timestamp = Math.floor((new Date).getTime()/1000);
    if (brandelli) {
      tag = "https://ad.adrequest.net/makerschannel/themctv/?pos=pre&child_friendly=false&theme=sports&description_url=" + window.location.href + "&correlator=" + timestamp + "&flash=true";
    } else {
      tag = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=short_onecue&correlator=" + timestamp;
    }
    return tag;
  };

  consolePrint = function(string) {
    console.log('%c ' + 'The MC: ' + (new Date()).toString() +  " " +  string + ' ' + ', video: ' + player.currentItem().title , 'background: #222; color: #bada55; font-size: 1.5em;');
  };

  sendToGoogleAnalytics = function(object) {
    dataLayer.push({
      "event": "player",
      "category": "Player event category",
      "action": object.eventType,
      "production_id": object.productionId,
      "production_title": object.productionTitle,
      "player_event": object.playerEvent,
      "progress": object.progress,
      "event_date": (new Date()).toString(),
      "browser": navigator.userAgent
    });
  };

  initCallback = function() {
    var player = this;

    events.forEach(function(eventType) {
      player.on(eventType, function(e) {
        var title = player.currentItem().title;
        var id    = player.currentItem().id;
        logBuffer.push((new Date()).toString() + ": " + eventType  + ", videoID: " + id + ", video: " + title);
        consolePrint(eventType);
        if (!devMode) {
          sendToGoogleAnalytics({
            eventType: eventType,
            productionId: id,
            productionTitle: title,
            playerEvent: eventType,
            progress: null
          });
        }
      })
    });

    player.on('itemstarted', function() {
      isItemPlaying = true;
      lastPercentile = 0;
      lastTime = 0;
    });

    player.on(['itemended', 'itemskipped'], function()
    {
      isItemPlaying = false;
    });

    player.on('timeupdate', function() {
      // Do not send any events when the content is still resuming or an ad is playing
      if (player.isAdPlaying && (player.isAdPlaying() || player.isContentResuming())) {
        return;
      }

      // Do not send any events when no item is playing
      if (!isItemPlaying) {
        return;
      }

      var currentTime = player.currentTime();
      var duration    = player.duration();

      // Track playing every 10%
      let percentile = Math.floor((currentTime / duration) * 10) * 10;

      // Ignore completely if the time difference is too big. This happens when timeupdate happens before or during seeking
      // video.js guarantees that a timeupdate is sent every 250ms, so if the time difference is more than 1.5s, we
      // will not send a progress event
      if (Math.abs(currentTime - lastTime) > 1.5) {
        // Act as if the percentile was already sent
        lastPercentile = percentile;
        lastTime = currentTime;
        return;
      }

      lastTime = currentTime;
      title = player.currentItem().title;
      id = player.currentItem().id;

      if (percentile != lastPercentile) {

        logBuffer.push((new Date()).toString() + ": progress " + percentile + "% , videoID: " + id + ", video: " + title);
        consolePrint("Progress " + percentile + "%");

        // Do not send events on 0%
        if (percentile > 0) {
          if (!devMode) {
            sendToGoogleAnalytics({
              eventType: "Progress",
              productionId: id,
              productionTitle: title,
              playerEvent: "Progress",
              progress: percentile
            });
          }
        }
        lastPercentile = percentile;
      }
    })
  };

  createDownloadableLog = function() {
    var textFile = null;
    makeTextFile = function (text) {
      var data = new Blob([text], {type: 'text/plain'});

      // If we are replacing a previously generated file we need to
      // manually revoke the object URL to avoid memory leaks.
      if (textFile !== null) {
        window.URL.revokeObjectURL(textFile);
      }

      textFile = window.URL.createObjectURL(data);

      // returns a URL you can use as a href
      return textFile;
    };

    var downloadLink = document.getElementById('download');

    downloadLink.addEventListener('click', function () {
      var link = document.createElement('a');
      link.setAttribute('download', 'event_log.txt');
      link.href = makeTextFile(logBuffer.join("\n"));
      document.body.appendChild(link);

      // wait for the link to be added to the document
      window.requestAnimationFrame(function () {
        var event = new MouseEvent('click');
        link.dispatchEvent(event);
        document.body.removeChild(link);
      });
    }, false);
  };

  setupPlaylistForPlayer = function(playlist) {
    playlist.forEach(function(playlistItem) {
      options.playlist.items.push(
        {
          id:         playlistItem.id,
          title:      playlistItem.title,
          poster:     playlistItem.poster,
          duration:   null,
          audioonly:  false,
          live:       false,
          account:    "account",
          adtag:      getAdTag(false),
          locations: {
            adaptive: [],
            progressive: [
              {
                label: "normal",
                sources:[
                  {
                    type: "video/mp4",
                    src: playlistItem.src
                  }
                ]
              }
            ]
          }
        }
      )
    })
  };

  return {
    init: function( playlist, developmentMode ) {

      // 1. Check for dev mode. If so, don't send data to Google
      devMode = developmentMode;

      // 2. Setup the playlist
      setupPlaylistForPlayer(playlist);

      // 3. Create the player
      player = StreamOnePlayer('video', options, initCallback);

      // 4. Create the downloadable link
      createDownloadableLog();
    },
    getPlayer: function() {
      return player;
    }
  }
})();
