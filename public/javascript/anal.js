var element = document.getElementById("some-button");

element.addEventListener('click', function() {
  if (mcDataLayer) {
    console.log("Hoppakee, posten maar");
    dataLayer.push({
      "event":                     "player",
      "category":                  "Player event category",
      "action":                    "Marvin's custom event action",
      "label":                     "Marvin's custom event label",
    });
  }
}, false);
