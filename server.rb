require 'sinatra'
require 'byebug'

use Rack::Auth::Basic, "Restricted Area" do |username, password|
  username == 'starboy' and password == 'analboy'
end

get '/' do
  erb :index
end

get '/testanal' do
  erb :testanal
end
